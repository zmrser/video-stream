import * as express from "express";
import * as log4js from "log4js";
import * as path from "path";
import * as https from "https";
import * as fs from "fs";
import * as child from 'child_process';

const log = log4js.getLogger("route");
log.level = "debug";

const expressRouter: express.Router = express.Router();
let ffmpeg: child.ChildProcessWithoutNullStreams;

expressRouter.post("/upload", async (req, res, next) => {
	log.info("POST upload...");
	const urlToFile: string = req.body.url;
	const fileName: string | undefined = urlToFile.split(/[\\\/]/).pop();
	const extension: string | undefined = urlToFile.match(/\.[0-9a-z]+$/i)![0];

	// Check valid url
	if (!urlToFile || !fileName) {
		return res.status(400).send("Is missing URL param in the request body.");
	}
	if (extension !== ".mp4") {
		return res.status(400).send("It is not the correct format file. expected mp4.");
	}

	try {
		await helpers.downloadByUrl(urlToFile, fileName);
		helpers.runStream(fileName);
	} catch (e) {
		log.error(e);
		return next(e);
	}

	const message = {
		status: "success",
		stream: `http://${req.host}:8089/${fileName.replace(extension, ".m3u8")}`,
	};
	res.status(200).send(message).end();
});

expressRouter.get("/:file", async (req, res, next) => {
	log.trace("GET /...");
	const fileName: string = req.path.substr(1);
	try {
		const pathFileM3u8: string = path.join(__dirname, "..", "..", "temp", fileName);

		fs.readFile(pathFileM3u8, function (err, contents) {
			if (err) {
				res.status(500);
				res.end();
			} else if (contents) {
				res.header(
					{ 'Content-Type': 'application/vnd.apple.mpegurl' })
					.status(200);
				res.end(contents, 'utf-8');
				log.info('send');
			} else {
				log.info('empty playlist');
				res.status(500);
				res.end();
			}
		});
	} catch (e) {
		log.error(e);
	}

	log.info('Done');
});

expressRouter.delete("/stop", async (req, res, next) => {
	log.info("DELETE stop...");

	try {
		await ffmpeg.kill('SIGINT');
		await new Promise(resolve => setTimeout(resolve, 1000));
		helpers.removeStreamFiles();
	} catch (e) {
		log.error(e);
		return next(e);
	}
	const message = {
		status: "success",
	};
	res.status(200).send(message).end();
});


export namespace helpers {
	export function downloadByUrl(url: string, fileName: string): Promise<string> {
		return new Promise((resolve, reject) => {

			const urlToFile: string = path.join(__dirname, "..", "..", "temp", fileName);
			const file = fs.createWriteStream(urlToFile);
			try {
				https.get(url, (response) => {
					response.pipe(file);
					return resolve(fileName);
				});
			} catch (e) {
				return reject(e);
			}
		});
	}

	export function runStream(fileName: string) {
		const options: Array<string> = [
			"-re",
			"-stream_loop",
			"-1",
			"-i",
			`${path.join(__dirname, '..', '..', 'temp', fileName)}`,
			"-c:a",
			"aac",
			"-c:v",
			"libx264",
			"-ar",
			"48000",
			"-b:a",
			"128k",
			"-keyint_min",
			"24",
			"-g",
			"24",
			"-r",
			"24",
			"-f",
			"hls",
			"-hls_list_size",
			"2",
			"-hls_time",
			"1",
			`${path.join(__dirname, '..', '..', 'temp', fileName.replace('.mp4', '.m3u8'))}`,
		];
		ffmpeg = child.spawn("ffmpeg", options);

		ffmpeg.stdout.on("data", (data: any) => {
			log.info(`stdout: ${data}`);
		});

		ffmpeg.stderr.on("data", (data: any) => {
			log.info(`stderr: ${data}`);
		});

		ffmpeg.on('error', (error: any) => {
			log.error(`error: ${error.message}`);
		});

		ffmpeg.on("close", (code: any) => {
			log.info(`child process exited with code ${code}`);
		});
	}

	export function removeStreamFiles() {
		const defaultDir: string = path.join(__dirname, '..', '..', 'temp');
		fs.readdir(defaultDir, (err, files) => {
			if (err) throw err;

			for (const file of files) {
				fs.unlink(path.join(defaultDir, file), err => {
					if (err) throw err;
				});
			}
		});
	}
}

export default expressRouter;
