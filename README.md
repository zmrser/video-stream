# How to run
```sh
$ npm install
$ npm run build
$ npm run start
```

## Http endpoints

### POST: /upload
- Download file and start stream live:
##### Request
```sh
curl --location --request POST 'http://127.0.0.1:8089/upload' \
--header 'Content-Type: application/json' \
--data-raw '{
	"url": "https://codeda.com/data/syncTest.mp4"
}'
```
##### Response
```sh
{
    "status": "success",
    "stream": "http://127.0.0.1:8089/syncTest.m3u8"
}
```
#### GET: /:file
- Endpoiter for connect to stream video.
##### Request
```sh
curl --location --request GET 'http://127.0.0.1:8089/stream.m3u8'
```

#### DELETE: /stop
- Stop ffmpeg and remove all files.

##### Request
```sh
curl --location --request DELETE 'http://127.0.0.1:8089/stop'
```

##### Response
```sh
{
    "status": "success"
}
```
